# rpi-simplex-clock

A Raspberry Pi based driver for 2-wire analog clocks that use the 24v Simplex/IBM reverse-polarity system.